# Malicious question detection

Research project in the field of NLP. Based on the the Kaggle challenge [Quora Insincere Questions Classification](https://www.kaggle.com/c/quora-insincere-questions-classification).

## Stage 2 - Comparison between large scale models

For the second stage of the project I will focus on comparing the performance of different large pretrained models and see if there is a pattern in the detection of malicious speech.
Main focus:
 - Is one model better at detecting particular types of malicious
 - How to best explore the complex large scale models like BERT

### Current task list

- [ ] Find a good way to split the dataset and make sure it will not lead to overfitting; the data split
should ensure that the experiment results are easily replicable; the result should also be logged and maybe
easy to be visualized
- [ ] Understand how HuggingFace libraries work and train BERT on the dataset
- [ ] Read the relevant papers
  - [ ] [XLNet](https://arxiv.org/pdf/1906.08237v1.pdf)
  - [ ] [RoBERTa](https://arxiv.org/pdf/1907.11692v1.pdf)
  - [ ] [CTRL](https://arxiv.org/pdf/1909.05858.pdf)
  - [ ] [DistilBERT](https://arxiv.org/pdf/1910.01108.pdf)


### State of the art | Related work

- [ ] [BERT Rediscovers the Classical NLP Pipeline](https://arxiv.org/pdf/1905.05950.pdf)
- [ ] [Are Sixteen Heads Really Better than One?](https://arxiv.org/pdf/1905.10650.pdf)
- [ ] [What Does BERT Look At? An Analysis of BERT's Attention](https://arxiv.org/pdf/1906.04341.pdf)
- [ ] [Probing for sentence structure in contextualized word representations](https://arxiv.org/pdf/1905.06316.pdf)



---

###Stage 1

- [x] Learn about **attention/self-attention** mechanisms
- [x] Refresh the basics: Softmax, MaxPooling, Back-Propagation
- [x] Learn about sentiment analysis in general
  - [x] What are common metrics
  - [x] Common methods used
  - [x] Anything else that is specific to the field and read 2 successful papers
- [x] Implement a baseline with two different models
- [ ] Read about Capsule networks and propose a new architecture using them
- [x] Learn about ELMo - read the paper and add to Part I
- [x] Implement confusion matrix visualisation
- [x] Write at the beginning of part two an introduction about the task and the dataset

- [x] Learn about GPT
- [x] Learn about BERT
- [ ] Learn about Transformer XL

---

### Paper links

#### General NLP

- [x] [Learning Phrase Representations using RNN Encoder–Decoder for Statistical Machine Translation](http://emnlp2014.org/papers/pdf/EMNLP2014179.pdf)
- [x] [Sequence to Sequence Learning with Neural Networks](https://papers.nips.cc/paper/5346-sequence-to-sequence-learning-with-neural-networks.pdf)
- [x] [Neural Machine Translation by Jointly Learning to Align and Translate](https://arxiv.org/pdf/1409.0473.pdf)
- [x] [Effective Approaches to Attention-based Neural Machine Translation](https://arxiv.org/pdf/1508.04025.pdf)
- [x] [Transformer](https://arxiv.org/pdf/1706.03762)
- [ ] [Dissecting contextual word embeddings: Architecture and representation](https://arxiv.org/pdf/1808.08949.pdf)
- [x] [ELMo](https://arxiv.org/pdf/1802.05365)
- [x] [BERT](https://arxiv.org/pdf/1810.04805.pdf)
- [x] [GPT](https://s3-us-west-2.amazonaws.com/openai-assets/research-covers/language-unsupervised/language_understanding_paper.pdf)
- [ ] [MT-DNN](https://arxiv.org/pdf/1901.11504v2.pdf)
- [ ] [Transformer XL](https://arxiv.org/pdf/1901.02860)
- [ ] [Cloze-driven Pretraining of Self-attention Networks](https://arxiv.org/pdf/1903.07785.pdf)
- [x] [XLNet](https://arxiv.org/pdf/1906.08237v1.pdf)
- [ ] [RoBERTa](https://arxiv.org/pdf/1907.11692v1.pdf)
- [ ] [ERNIE](https://arxiv.org/pdf/1907.12412v1.pdf)

#### Toxic comment detection

- [x] [Analyzing Labeled Cyberbullying Incidentson the Instagram Social Network](https://www.cs.colorado.edu/~rhan/Papers/socinfo2015_labeled.pdf)
- [x] [Abusive Language Detection in Online User Content](http://www.yichang-cs.com/yahoo/WWW16_Abusivedetection.pdf)
- [x] [Anyone Can Become a Troll: Causes of Trolling Behavior in Online Discussions](https://arxiv.org/abs/1702.01119.pdf)
- [ ] [Deceiving Google's Perspective API Built for Detecting Toxic Comments](https://arxiv.org/abs/1702.08138.pdf)
- [x] [Ex Machina: Personal Attacks Seen at Scale](https://arxiv.org/pdf/1610.08914.pdf)
- [x] [Automated Hate Speech Detection and the Problem of Offensive Language](https://arxiv.org/pdf/1703.04009.pdf)
- [ ] [Shielding Google's language toxicity model against adversarial attacks](https://arxiv.org/pdf/1801.01828v1.pdf)
- [x] [Convolutional Neural Networks for Toxic Comment Classification](https://arxiv.org/pdf/1802.09957v1.pdf)
- [x] [Challenges for Toxic Comment Classification: An In-Depth Error Analysis](https://arxiv.org/pdf/1809.07572v1.pdf)
- [x] [Detecting Hate Speech and Offensive Language on Twitter using Machine Learning: An N-gram and TFIDF based Approach](https://arxiv.org/pdf/1809.08651v1.pdf)
- [x] [Stop Illegal Comments: A Multi-Task Deep Learning Approach](https://arxiv.org/pdf/1810.06665v1.pdf)
- [x] [Detecting Offensive Content in Open-domain Conversations using Two Stage Semi-supervision](https://arxiv.org/pdf/1811.12900v1.pdf)
- [ ] [Impact of Sentiment Detection to Recognize Toxic and Subversive Online Comments](https://arxiv.org/pdf/1812.01704v1.pdf)
- [ ] [Improving Sentiment Analysis with Multi-task Learning of Negation](https://arxiv.org/pdf/1906.07610.pdf)
- [x] Textual sentiment analysis via three different attention convolutional neural networks and cross-modality consistent regression

### Links with information
- [Attention](https://medium.com/@Alibaba_Cloud/self-attention-mechanisms-in-natural-language-processing-9f28315ff905)
- [BERT & GPT-2](https://www.topbots.com/generalized-language-models-bert-openai-gpt2/)
- [Interesting list of papers](https://github.com/jojonki/arXivNotes/issues)
- [Bahdanau-Luong Attention model comparison](http://cnyah.com/2017/08/01/attention-variants/)
- [Conversation AI research resources](https://conversationai.github.io/research.html)
- [Jigsaw article - Algorithms And Insults: Scaling Up Our Understanding Of Harassment On Wikipedia](https://medium.com/jigsaw/algorithms-and-insults-scaling-up-our-understanding-of-harassment-on-wikipedia-6cc417b9f7ff)
- [Google research - Attacking discrimination with smarter machine learning](https://research.google.com/bigpicture/attacking-discrimination-in-ml/)
- [Paper - Measuring and Mitigating Unintended Bias in Text Classification](https://github.com/conversationai/unintended-ml-bias-analysis/blob/master/presentations/measuring-mitigating-unintended-bias-paper.pdf)
- [Paper - Graph-based features for automatic online abuse detection](https://arxiv.org/pdf/1708.01060.pdf)
- [PyTorch implementation of Transformer](http://nlp.seas.harvard.edu/2018/04/03/attention.html)
- Wikipeida toxic terms: [Disability](https://en.wikipedia.org/wiki/List_of_disability-related_terms_with_negative_connotations), [Ethnic](https://en.wikipedia.org/wiki/List_of_ethnic_slurs), [LGBT](https://en.wikipedia.org/wiki/List_of_LGBT_slang_terms)
- [About AUC - ROC](https://towardsdatascience.com/understanding-auc-roc-curve-68b2303cc9c5)
- [Nice wordclouds](https://minimaxir.com/2016/05/wordclouds/)
