import math

from keras import Model, Input
from keras.engine import Layer
from keras.layers import Bidirectional, CuDNNLSTM, Embedding, CuDNNGRU, GlobalMaxPool1D, Dropout, PReLU, Conv1D, \
    SpatialDropout1D
from keras_contrib.callbacks import CyclicLR
from keras_preprocessing.sequence import pad_sequences
from keras_preprocessing.text import Tokenizer
from sklearn import metrics
from sklearn.model_selection import train_test_split

from data import Data
from visualization import ModelVisualization

from pprint import pprint
import numpy as np
import pandas as pd
import json
import tensorflow_hub as hub
import keras.backend as K
import tensorflow as tf

from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras import initializers, regularizers, constraints


class DataRepresentation(object):
    config = {
        'embedding_type': 'glove',  # tfidf, glove, google, wiki, paragram, elmo
        'max_features': 95000,  # how many unique words to use (i.e num rows in embedding vector)
        'embed_size': 300,  # how big is each word vector
        'max_len': 100,  # max number of words in a question to use
        'random_seed': 42,
        'test_split_size': 0.1,  # portion of training text that is split to be used for validation
        'balanced': False
    }
    '''
    Pipeline:
    Get the data + embeddings
    -> tokenize the text so each word is easy to access
    -> create an embedding matrix with the sequence representation using the words form the text
        the matrix should be initialized either with mean values or random ones for the words that
        do not have a representation
    -> ?
    '''

    @classmethod
    def configure_representation(cls, config):
        for k in config:
            cls.config[k] = config[k]
        cls.__split_training_text()
        cls.__tokenize_and_embed()
        if cls.config['embedding_type'] == 'tfidf':
            cls.__tfidf_embeddings()
        elif cls.config['embedding_type'] == 'elmo':
            print('no idea')
        elif cls.config['embedding_type'] in ['word2vec', 'fasttext', 'paragram', 'glove', 'toy']:
            Data.load_embeddings(cls.config['embedding_type'])
            cls.__pretrained_embeddings(cls.config['embedding_type'])
            print('Embeddings loaded.')
        else:
            print('Embedding type not supported')
            exit(388)
        print('train_x:', cls.train_x.shape)
        print('train_y:', cls.train_y.shape)
        print('val_x:', cls.val_x.shape)
        print('val_y:', cls.val_y.shape)
        print('test_x:', cls.test_x.shape)

    @classmethod
    def __split_training_text(cls):
        if cls.config['balanced']:
            print('Using a balanced set')

            balanced_set = pd.concat(
                [Data.train_raw[Data.train_raw['target'] == 0].sample(int(cls.config['max_features'] / 2)),
                 Data.train_raw[Data.train_raw['target'] == 1].sample(int(cls.config['max_features'] / 2))],
                ignore_index=True)
            print(balanced_set[0:2])
            print(balanced_set[-2:])
            print(balanced_set.shape)
            balanced_set = balanced_set.sample(frac=1).reset_index(drop=True)
            cls.train_df, cls.val_df = train_test_split(balanced_set, test_size=cls.config['test_split_size'],
                                                        random_state=42)
        else:
            print('Using an unbalanced set')
            cls.train_df, cls.val_df = train_test_split(Data.train_raw, test_size=cls.config['test_split_size'],
                                                        random_state=42)
        cls.train_df.reset_index(drop=True, inplace=True)
        cls.val_df.reset_index(drop=True, inplace=True)
        cls.train_y = cls.train_df['target'].values
        cls.val_y = cls.val_df['target'].values
        print(f'Shape of train: {cls.train_df.shape} and shape of val: {cls.val_df.shape}')

    @classmethod
    def __tokenize_and_embed(cls):
        cls.train_x = cls.train_df['question_text'].fillna('_na_').values
        cls.val_x = cls.val_df['question_text'].fillna('_na_').values
        cls.test_x = Data.test_raw['question_text'].fillna('_na_').values

        cls.tokenizer = Tokenizer(num_words=cls.config['max_features'])
        cls.tokenizer.fit_on_texts(list(cls.train_x))

        cls.train_x = cls.tokenizer.texts_to_sequences(cls.train_x)
        # add the extra features to the embedding
        # for idx, vec in enumerate(cls.train_x):
        #     cls.train_x[idx] = [
        #         cls.train_df.total_length.values[idx],
        #         cls.train_df.num_words.values[idx],
        #         cls.train_df.num_unique_words.values[idx],
        #         cls.train_df.num_unique_words.values[idx]] + cls.train_x[idx]
        cls.val_x = cls.tokenizer.texts_to_sequences(cls.val_x)
        cls.test_x = cls.tokenizer.texts_to_sequences(cls.test_x)

        cls.train_x = pad_sequences(cls.train_x, maxlen=cls.config['max_len'])
        cls.val_x = pad_sequences(cls.val_x, maxlen=cls.config['max_len'])
        cls.test_x = pad_sequences(cls.test_x, maxlen=cls.config['max_len'])

    @classmethod
    def __tfidf_embeddings(cls):
        cls.train_x = cls.tokenizer.texts_to_sequences(cls.train_x)
        cls.val_x = cls.tokenizer.texts_to_sequences(cls.val_x)
        cls.test_x = cls.tokenizer.texts_to_sequences(cls.test_x)

        cls.train_x = pad_sequences(cls.train_x, maxlen=cls.config['max_len'])
        cls.val_x = pad_sequences(cls.val_x, maxlen=cls.config['max_len'])
        cls.test_x = pad_sequences(cls.test_x, maxlen=cls.config['max_len'])

    @classmethod
    def __extra_metrics_to_vector(cls, idx):
        pass

    @classmethod
    def __pretrained_embeddings(cls, embedding_type='wiki'):
        if embedding_type in Data.embeddings:
            all_embs = np.stack(Data.embeddings[embedding_type].vectors)
            emb_mean, emb_std = all_embs.mean(), all_embs.std()
            # embed_size = all_embs.shape[1]
            word_index = cls.tokenizer.word_index
            nb_words = min(cls.config['max_features'], len(word_index))
            print(
                f'nb_words:{nb_words} | max_featues: {cls.config["max_features"]} | embed size: {cls.config["embed_size"]}')
            cls.embedding_matrix = np.random.normal(emb_mean, emb_std, (nb_words, cls.config['embed_size']))
            for word, i in word_index.items():
                if i >= cls.config['max_features']: continue
                if word in Data.embeddings[embedding_type].vocab:
                    # print(f'added word: {word}')
                    cls.embedding_matrix[i] = Data.embeddings[embedding_type].get_vector(word)
        else:
            print('Embeddings not initialized.')
            exit(388)
        print('Pretrained embedding load complete')
        print(cls.embedding_matrix.shape)

    @classmethod
    def __extract_text_from_results(cls):
        pass


class ResultLogger(object):
    logs_path = '/home/andrei/quora-research/logs/'

    @classmethod
    def model_log_result(cls, result, config):
        log_filename = ResultLogger.logs_path + 'model_' + config['model_name'] + '_' + config['embedding_type']
        out_df = pd.DataFrame({'qid': Data.test_raw['qid'].values})
        out_df['prediction'] = result
        out_df.to_csv(log_filename + '_submission.csv', index=False)
        with open(log_filename + '_config.json', 'w') as logh:
            json.dump(config, logh)

    @classmethod
    def log_false_negatives(cls, data, name, threshold):
        with open(f'{cls.logs_path}{name}_fn.log', 'w') as logh:
            for idx, dp in enumerate(data):
                dp = (dp > threshold).astype(int)
                if dp != DataRepresentation.val_y[idx] and dp == 0:
                    logh.write(DataRepresentation.val_df['question_text'][idx] + '\n')


class BiGRUModel(object):
    # https://keras.io/getting-started/sequential-model-guide/
    name = 'BiGRU'

    @classmethod
    def set_hyperparameters(cls, optimizer='adam', activation='sigmoid', batch_size=256,
                            loss_function='binary_crossentropy', metrics=['accuracy'],
                            epochs=5, steps_per_epoch=5000):
        cls.optimizer = optimizer
        cls.activation = activation
        cls.loss_function = loss_function
        cls.metrics = metrics
        cls.batch_size = batch_size
        cls.epochs = epochs
        cls.steps_per_epoch = steps_per_epoch

    @classmethod
    def __build_bilstm(cls):
        cls.model = Sequential()
        cls.model.add(Embedding(
            DataRepresentation.config['max_features'],
            DataRepresentation.config['embed_size'],
            weights=[DataRepresentation.embedding_matrix],
            input_length=DataRepresentation.config['max_len']))
        cls.model.add(Bidirectional(CuDNNGRU(64, return_sequences=True)))
        cls.model.add(GlobalMaxPool1D())
        cls.model.add(Dense(16, activation=cls.activation))
        cls.model.add(Dropout(0.1))
        cls.model.add(Dense(1, activation=cls.activation))
        cls.model.compile(loss=cls.loss_function, optimizer=cls.optimizer, metrics=cls.metrics)
        print('Model compiled.')
        print(cls.model.summary())

    @classmethod
    def run(cls):
        print('train x:', DataRepresentation.train_x.shape)
        print('train y:', DataRepresentation.train_y.shape)
        print('val y:', DataRepresentation.val_y.shape)
        print('val x:', DataRepresentation.val_x.shape)
        cls.__build_bilstm()
        cls.model.fit(
            DataRepresentation.train_x,
            DataRepresentation.train_y,
            batch_size=cls.batch_size,
            epochs=cls.epochs,
            validation_data=(DataRepresentation.val_x, DataRepresentation.val_y))

        best_threshold = 0.1
        best_score = 0
        predicted_val_y = cls.model.predict([DataRepresentation.val_x], batch_size=1024, verbose=1)
        for thresh in np.arange(0.1, 0.501, 0.01):
            thresh = np.round(thresh, 2)
            f1_score = metrics.f1_score(DataRepresentation.val_y, (predicted_val_y > thresh).astype(int))
            if f1_score > best_score:
                best_score, best_threshold = f1_score, thresh

        ResultLogger.log_false_negatives(predicted_val_y, f'{cls.name}_{DataRepresentation.config["embedding_type"]}',
                                         best_threshold)

        cm = metrics.confusion_matrix(DataRepresentation.val_y, (predicted_val_y > best_threshold).astype(int))
        print(f'Confusion matrix for {cls.name}: {cm}')
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print(f'Normalized confusion matrix for {cls.name}: {cm}')

        ModelVisualization.plot_confusion_matrix(
            cm, f'{cls.name} with {DataRepresentation.config["embedding_type"]} embeddings. F1: ' + '{0:.3f}'.format(best_score))

        # ModelVisualization.generate_model_diagram(cls.model, cls.name + DataRepresentation.config['embedding_type'])

        # predicted_test_y = cls.model.predict([DataRepresentation.test_x], batch_size=1024, verbose=1)
        # predicted_test_y = (predicted_test_y > best_threshold).astype(int)
        #
        # log_info = DataRepresentation.config
        # log_info['model_name'] = cls.name
        # log_info['f1_score'] = best_score
        # log_info['optimizer'] = cls.optimizer
        # log_info['loss'] = cls.loss_function
        # log_info['batch_size'] = cls.batch_size
        # ResultLogger.model_log_result(predicted_test_y, log_info)


class BiLSTMModel(object):
    # https://keras.io/getting-started/sequential-model-guide/
    name = 'BiLSTM'

    @classmethod
    def set_hyperparameters(cls, optimizer='adam', activation='sigmoid', batch_size=256,
                            loss_function='binary_crossentropy', metrics=['accuracy'],
                            epochs=5, steps_per_epoch=5000):
        cls.optimizer = optimizer
        cls.activation = activation
        cls.loss_function = loss_function
        cls.metrics = metrics
        cls.batch_size = batch_size
        cls.epochs = epochs
        cls.steps_per_epoch = steps_per_epoch

    @classmethod
    def __build_bilstm(cls):
        cls.model = Sequential()
        cls.model.add(Embedding(
            DataRepresentation.config['max_features'],
            DataRepresentation.config['embed_size'],
            weights=[DataRepresentation.embedding_matrix],
            input_length=DataRepresentation.config['max_len']))
        cls.model.add(Bidirectional(CuDNNLSTM(64, return_sequences=True)))
        cls.model.add(GlobalMaxPool1D())
        cls.model.add(Dense(16, activation=cls.activation))
        cls.model.add(Dropout(0.1))
        cls.model.add(Dense(1, activation=cls.activation))
        cls.model.compile(loss=cls.loss_function, optimizer=cls.optimizer, metrics=cls.metrics)
        print('Model compiled.')
        print(cls.model.summary())

    @classmethod
    def run(cls):
        print('train x:', DataRepresentation.train_x.shape)
        print('train y:', DataRepresentation.train_y.shape)
        print('val y:', DataRepresentation.val_y.shape)
        print('val x:', DataRepresentation.val_x.shape)
        cls.__build_bilstm()
        cls.model.fit(
            DataRepresentation.train_x,
            DataRepresentation.train_y,
            batch_size=cls.batch_size,
            epochs=cls.epochs,
            validation_data=(DataRepresentation.val_x, DataRepresentation.val_y))

        best_threshold = 0.1
        best_score = 0
        predicted_val_y = cls.model.predict([DataRepresentation.val_x], batch_size=1024, verbose=1)
        for thresh in np.arange(0.1, 0.501, 0.01):
            thresh = np.round(thresh, 2)
            f1_score = metrics.f1_score(DataRepresentation.val_y, (predicted_val_y > thresh).astype(int))
            if f1_score > best_score:
                best_score, best_threshold = f1_score, thresh

        ResultLogger.log_false_negatives(predicted_val_y, f'{cls.name}_{DataRepresentation.config["embedding_type"]}',
                                         best_threshold)

        cm = metrics.confusion_matrix(DataRepresentation.val_y, (predicted_val_y > best_threshold).astype(int))
        print(f'Confusion matrix for {cls.name}: {cm}')
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print(f'Normalized confusion matrix for {cls.name}: {cm}')

        ModelVisualization.plot_confusion_matrix(
            cm, f'{cls.name} with {DataRepresentation.config["embedding_type"]} embeddings. F1: ' + '{0:.3f}'.format(best_score))

        # predicted_test_y = cls.model.predict([DataRepresentation.test_x], batch_size=1024, verbose=1)
        # predicted_test_y = (predicted_test_y > best_threshold).astype(int)

        # log_info = DataRepresentation.config
        # log_info['model_name'] = cls.name
        # log_info['f1_score'] = best_score
        # log_info['optimizer'] = cls.optimizer
        # log_info['loss'] = cls.loss_function
        # log_info['batch_size'] = cls.batch_size
        # ResultLogger.model_log_result(predicted_test_y, log_info)


class AttentionModel(object):
    name = 'attn'

    @classmethod
    def set_hyperparameters(cls, optimizer='adam', activation='sigmoid', batch_size=256,
                            loss_function='binary_crossentropy', metrics=['accuracy'],
                            epochs=5, steps_per_epoch=5000):
        cls.optimizer = optimizer
        cls.activation = activation
        cls.loss_function = loss_function
        cls.metrics = metrics
        cls.batch_size = batch_size
        cls.epochs = epochs
        cls.steps_per_epoch = steps_per_epoch

    # source of the Attention layer:
    # https://www.kaggle.com/qqgeogor/keras-lstm-attention-glove840b-lb-0-043
    class Attention(Layer):
        def __init__(self, step_dim,
                     W_regularizer=None, b_regularizer=None,
                     W_constraint=None, b_constraint=None,
                     bias=True, **kwargs):
            """
            Keras Layer that implements an Attention mechanism for temporal data.
            Supports Masking.
            Follows the work of Raffel et al. [https://arxiv.org/abs/1512.08756]
            # Input shape
                3D tensor with shape: `(samples, steps, features)`.
            # Output shape
                2D tensor with shape: `(samples, features)`.
            :param kwargs:
            Just put it on top of an RNN Layer (GRU/LSTM/SimpleRNN) with return_sequences=True.
            The dimensions are inferred based on the output shape of the RNN.
            Example:
                model.add(LSTM(64, return_sequences=True))
                model.add(Attention())
            """
            self.supports_masking = True
            # self.init = initializations.get('glorot_uniform')
            self.init = initializers.get('glorot_uniform')

            self.W_regularizer = regularizers.get(W_regularizer)
            self.b_regularizer = regularizers.get(b_regularizer)

            self.W_constraint = constraints.get(W_constraint)
            self.b_constraint = constraints.get(b_constraint)

            self.bias = bias
            self.step_dim = step_dim
            self.features_dim = 0
            super(AttentionModel.Attention, self).__init__(**kwargs)

        def build(self, input_shape):
            assert len(input_shape) == 3

            self.W = self.add_weight((input_shape[-1],),
                                     initializer=self.init,
                                     name='{}_W'.format(self.name),
                                     regularizer=self.W_regularizer,
                                     constraint=self.W_constraint)
            self.features_dim = input_shape[-1]

            if self.bias:
                self.b = self.add_weight((input_shape[1],),
                                         initializer='zero',
                                         name='{}_b'.format(self.name),
                                         regularizer=self.b_regularizer,
                                         constraint=self.b_constraint)
            else:
                self.b = None

            self.built = True

        def compute_mask(self, input, input_mask=None):
            # do not pass the mask to the next layers
            return None

        def call(self, x, mask=None):
            # eij = K.dot(x, self.W) TF backend doesn't support it

            # features_dim = self.W.shape[0]
            # step_dim = x._keras_shape[1]

            features_dim = self.features_dim
            step_dim = self.step_dim

            eij = K.reshape(K.dot(K.reshape(x, (-1, features_dim)), K.reshape(self.W, (features_dim, 1))),
                            (-1, step_dim))

            if self.bias:
                eij += self.b

            eij = K.tanh(eij)

            a = K.exp(eij)

            # apply mask after the exp. will be re-normalized next
            if mask is not None:
                # Cast the mask to floatX to avoid float64 upcasting in theano
                a *= K.cast(mask, K.floatx())

            # in some cases especially in the early stages of training the sum may be almost zero
            a /= K.cast(K.sum(a, axis=1, keepdims=True) + K.epsilon(), K.floatx())

            a = K.expand_dims(a)
            weighted_input = x * a
            # print weigthted_input.shape
            return K.sum(weighted_input, axis=1)

        def compute_output_shape(self, input_shape):
            # return input_shape[0], input_shape[-1]
            return input_shape[0], self.features_dim

    @classmethod
    def __build_attention(cls):
        cls.model = Sequential()
        cls.model.add(Embedding(
            DataRepresentation.config['max_features'],
            DataRepresentation.config['embed_size'],
            weights=[DataRepresentation.embedding_matrix],
            input_length=DataRepresentation.config['max_len']))
        cls.model.add(Bidirectional(CuDNNLSTM(128, return_sequences=True)))
        cls.model.add(Bidirectional(CuDNNGRU(64, return_sequences=True)))
        cls.model.add(Conv1D(64, kernel_size=1, kernel_initializer=initializers.he_uniform(seed=42), activation="tanh"))
        cls.model.add(cls.Attention(DataRepresentation.config['max_len']))
        cls.model.add(Dense(256, activation='relu'))
        cls.model.add(Dense(1, activation='sigmoid'))
        cls.model.compile(loss=AttentionModel.loss_function,
                          optimizer=AttentionModel.optimizer,
                          metrics=AttentionModel.metrics)
        ModelVisualization.generate_model_diagram(cls.model, cls.name + DataRepresentation.config['embedding_type'])

    @classmethod
    def run(cls):
        print(f'Running f{cls.name} model.')
        cls.__build_attention()
        cls.model.fit(
            DataRepresentation.train_x,
            DataRepresentation.train_y,
            batch_size=AttentionModel.batch_size,
            epochs=AttentionModel.epochs,
            validation_data=(DataRepresentation.val_x, DataRepresentation.val_y))

        best_threshold = 0.1
        best_score = 0
        predicted_val_y = cls.model.predict([DataRepresentation.val_x], batch_size=1024, verbose=1)
        for thresh in np.arange(0.1, 0.501, 0.01):
            thresh = np.round(thresh, 2)
            f1_score = metrics.f1_score(DataRepresentation.val_y, (predicted_val_y > thresh).astype(int))
            if f1_score > best_score:
                best_score, best_threshold = f1_score, thresh

        ResultLogger.log_false_negatives(predicted_val_y, f'{cls.name}_{DataRepresentation.config["embedding_type"]}',
                                         best_threshold)

        print(f'Best score is {best_score} at {best_threshold}.')
        cm = metrics.confusion_matrix(DataRepresentation.val_y, (predicted_val_y > best_threshold).astype(int))
        print(f'Confusion matrix for {cls.name}: {cm}')
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print(f'Normalized confusion matrix for {cls.name}: {cm}')

        ModelVisualization.plot_confusion_matrix(
            cm, f'{cls.name} with {DataRepresentation.config["embedding_type"]} embeddings. F1: ' + '{0:.3f}'.format(best_score))


class ElmoEmbeddingLayer(Layer):
    # source: https://www.kaggle.com/yohalf/trying-the-power-of-elmo
    def __init__(self, trainable=True, **kwargs):
        self.dimensions = 1024
        self.trainable = trainable
        super(ElmoEmbeddingLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        self.elmo = hub.Module('https://tfhub.dev/google/elmo/2', trainable=self.trainable,
                               name="{}_module".format(self.name))
        self.trainable_weights += K.tf.trainable_variables(scope="^{}_module/.*".format(self.name))
        super(ElmoEmbeddingLayer, self).build(input_shape)

    def call(self, x, mask=None):
        result = self.elmo(K.squeeze(K.cast(x, tf.string), axis=1),
                           as_dict=True,
                           signature='default',
                           )['default']
        return result

    def compute_mask(self, inputs, mask=None):
        return K.not_equal(inputs, '--PAD--')

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.dimensions)


class ElmoModel(object):
    # about CyclicLR https://www.datacamp.com/community/tutorials/cyclical-learning-neural-nets
    @classmethod
    def __build_model(cls):
        inp = Input(shape=(None, 70, 300), name='input')
        embedding = ElmoEmbeddingLayer()(inp)
        dense = Dense(512)(embedding)
        dense = PReLU()(dense)
        dense = Dropout(0.3)(dense)
        dense = Dense(256)(dense)
        dense = PReLU()(dense)
        dense = Dropout(0.3)(dense)
        pred = Dense(1, activation='sigmoid')(dense)
        model = Model(inputs=inp, outputs=pred)
        model.compile(loss='binary_crossentropy', metrics=['accuracy'], optimizer='adam')
        return model

    @classmethod
    def run(cls):
        cls.__build_model()
        K.clear_session()
        model = cls.gen_model()
        model.summary()

        batch_size = 512
        epochs = 4
        step_size = int(int(len(DataRepresentation.train_x) / batch_size) * epochs / 2)
        cb = [
            CyclicLR(5e-4, 2e-3, step_size)
        ]

        model.fit(DataRepresentation.train_x,
                  DataRepresentation.train_y,
                  validation_data=(DataRepresentation.val_x, DataRepresentation.val_y),
                  epochs=epochs,
                  batch_size=batch_size,
                  verbose=1,
                  shuffle=True,
                  callbacks=cb)

        predict_test_y = cls.model.predict(DataRepresentation.test_x, verbose=1).argmax(axis=1)

        log_info = DataRepresentation.config
        log_info['model_name'] = cls.name
        # log_info['f1_score'] = best_score
        log_info['optimizer'] = cls.optimizer
        log_info['loss'] = cls.loss_function
        log_info['batch_size'] = cls.batch_size
        ResultLogger.model_log_result(predict_test_y)


class Transformer(object):
    # https://www.kaggle.com/shujian/transformer-with-lstm
    pass


class BERT(object):
    pass
