import json
import operator
import pandas as pd
import re
import numpy as np
from collections import defaultdict
from gensim.models import KeyedVectors
from pprint import pprint
from tqdm import tqdm
from wordcloud import STOPWORDS


class Data(object):
    train_raw = pd.DataFrame()
    test_raw = pd.DataFrame()
    data_path = '/home/andrei/quora-research/data/'
    embeddings_path = '/home/andrei/quora-research/data/embeddings/'
    logs_path = '/home/andrei/quora-research/logs/'
    embeddings = {}
    augmented = False

    @classmethod
    def get_train_data(cls):
        return cls.train_raw

    @classmethod
    def get_train_sincere(cls):
        return cls.train_raw[cls.train_raw['target'] == 0]

    @classmethod
    def get_train_insincere(cls):
        return cls.train_raw[cls.train_raw['target'] == 1]

    @classmethod
    def show_embedding_info(cls, embedding_type):
        if embedding_type not in cls.embeddings.keys():
            print('Embedding type not initialized.')
        print(f'Selected embedding: {embedding_type}')
        print('Vocabulary Size: {0}'.format(len(cls.embeddings[embedding_type].vocab)))
        print('First 10 words:')
        first_words = []
        for i, w in enumerate(cls.embeddings[embedding_type].vocab):
            print(w)
            first_words.append(w)
            if i > 10:
                break
        print(f'Embedding shape:{cls.embeddings[embedding_type][first_words[0]].shape}')

    @classmethod
    def load_embeddings(cls, emb_type):
        if emb_type == 'glove':
            cls.embeddings['glove'] = KeyedVectors\
                .load_word2vec_format(cls.embeddings_path +
                                      'glove.840B.300d/glove.840B.300d.bin',
                                      unicode_errors='replace', binary=True)
        elif emb_type == 'paragram':
            cls.embeddings['paragram'] = KeyedVectors \
                .load_word2vec_format(cls.embeddings_path +
                                      'paragram_300_sl999/paragram_300_sl999.bin',
                                      unicode_errors='replace', binary=True)
        elif emb_type == 'fasttext':
            cls.embeddings['fasttext'] = KeyedVectors \
                .load_word2vec_format(cls.embeddings_path +
                                      'wiki-news-300d-1M/wiki-news-300d-1M.bin', unicode_errors='replace',
                                      binary=True)
        elif emb_type == 'word2vec':
            cls.embeddings['word2vec'] = KeyedVectors \
                .load_word2vec_format(cls.embeddings_path +
                                      'GoogleNews-vectors-negative300/GoogleNews-vectors-negative300.bin',
                                      binary=True)
        elif emb_type == 'toy':
            cls.embeddings['toy'] = KeyedVectors \
                .load_word2vec_format(cls.embeddings_path + 'glove.840B.300d/glove.toy.txt')

    @classmethod
    def load_data(cls, data_set='all'):
        if data_set == 'all':
            cls.train_raw = pd.read_csv(cls.data_path + 'train.csv')
            cls.test_raw = pd.read_csv(cls.data_path + 'test.csv')
        elif data_set == 'train':
            cls.train_raw = pd.read_csv(cls.data_path + 'train.csv')
        elif data_set == 'test':
            cls.test_raw = pd.read_csv(cls.data_path + 'test.csv')
        print('Finished loading data.')

    @classmethod
    def load_preprocessed(cls, balance=False, aug=False):
        if aug:
            cls.train_raw = pd.read_csv(cls.data_path + 'pre_aug_train.csv')
            cls.test_raw = pd.read_csv(cls.data_path + 'pre_aug_test.csv')
        else:
            cls.train_raw = pd.read_csv(cls.data_path + 'pre_train.csv')
            cls.test_raw = pd.read_csv(cls.data_path + 'pre_test.csv')

        # if balance:
        #     ins_q_count = len(cls.train_raw[cls.train_raw['target'] == 1])
        #     balanced_set = cls.train_raw[cls.train_raw['target'] == 0].sample(ins_q_count)
        print('Finished loading pre-processed data.')

    @classmethod
    def augment_features(cls):
        for dataset in [cls.train_raw, cls.test_raw]:
            dataset['total_length'] = dataset['question_text'].apply(len)
            dataset['capitals'] = dataset['question_text'].apply(lambda comment: sum(1 for c in comment if c.isupper()))
            dataset['caps_vs_length'] = dataset.apply(lambda row: float(row['capitals']) / float(row['total_length']),
                                                     axis=1)
            dataset['num_words'] = dataset.question_text.str.count('\S+')
            dataset['num_unique_words'] = dataset['question_text'].apply(
                lambda comment: len(set(w for w in comment.split())))
            dataset['words_vs_unique'] = dataset['num_unique_words'] / cls.train_raw['num_words']

        cls.augmented = True
        print('Finished augmenting data with features')

    @classmethod
    def __save_preprocessed_corpus(cls):
        if cls.augmented:
            name_prefix = 'pre_aug_'
        else:
            name_prefix = 'pre_'
        cls.train_raw.to_csv(f'{cls.data_path}{name_prefix}train.csv', index=None, header=True)
        cls.test_raw.to_csv(f'{cls.data_path}{name_prefix}test.csv', index=None, header=True)

    @classmethod
    def __clean_string(cls, token):
        # if re.match(r'.*[https?://]', token) or re.match(r'[www\.]', token):
        #     return ''
        token = token.lower()
        token = re.sub(r'[/,?!&-]', ' ', token)
        if re.match(r'[a-z]{2,}[.][a-z]{2,}', token):
            token = re.sub(r'[.]', ' ', token) # remove improper sentence delimitation while keeping acronyms
        token = re.sub(r'[^\w ]', '', token)  # remove non-alphanum
        token = re.sub(r'[^\x00-\x7f]', '', token)  # remove non-ascii
        token = re.sub(r' [0-9]+ |^[0-9]+ | [0-9]+$|^[0-9]+$', ' ', token)  # remove numbers
        token = re.sub(r' [ ]+', ' ', token)
        token = token.strip()
        token = re.sub(r'^a |^of |^to ', '', token)

        return token

    @classmethod
    def clean_non_ascii_voc(cls):
        cls.train_raw['question_text'] = cls.train_raw['question_text'].apply(lambda x: cls.__clean_string(x))
        cls.train_raw = cls.train_raw[cls.train_raw.question_text != '']
        cls.test_raw['question_text'] = cls.test_raw['question_text'].apply(lambda x: cls.__clean_string(x))
        cls.test_raw = cls.test_raw[cls.test_raw.question_text != '']
        print('Non-ASCII cleaning complete.')

    @classmethod
    def get_word_frequency(cls, df=None):
        freq_dict = defaultdict(int)
        for sentence in df.apply(lambda x: x.split(' ')):
            for word in sentence:
                freq_dict[word] += 1
        return freq_dict

    @classmethod
    def vocabulary_embedding_overlap(cls, embedding_type='word2vec'):
        cls.load_embeddings(embedding_type)
        overlap = 0
        total = 0
        not_found = defaultdict(int)
        for sentence in tqdm(cls.train_raw['question_text'].apply(lambda x: x.strip().split(' '))):
            for word in sentence:
                if word in cls.embeddings[embedding_type]:
                    overlap += 1
                else:
                    not_found[word] += 1
                total += 1
        print('Number of not found words: ', total)
        print('Percent of words in embeddings: ', overlap * 100 / total)
        print('Percent of vocabulary in embeddings', 100 - len(not_found) * 100 / total)
        with open(cls.logs_path + 'embedding_overlap_' + embedding_type + '_not_found.json', 'w') as logh:
            json.dump(not_found, logh, indent=4)
        print('Most common words not in embeddings:')
        pprint(cls.get_sorted_list(not_found)[:20])

    @classmethod
    def generate_ngrams(cls, text, n_gram=1):
        token = [token for token in text.lower().split(' ') if token != '' if token not in STOPWORDS]
        ngrams = zip(*[token[i:] for i in range(n_gram)])
        return [" ".join(ngram) for ngram in ngrams]

    @classmethod
    def get_sorted_list(cls, dict_data):
        return sorted(dict_data.items(), key=operator.itemgetter(1))[::-1]

    @classmethod
    def n_gram_summary(cls, n, data, flag='', top_size=0):
        with open(cls.logs_path + 'n_gram_' + str(n) + '_' + flag + '.json', 'w') as logh:
            freq_dict = defaultdict(int)
            for point in data['question_text']:
                for ngram in cls.generate_ngrams(point, n):
                    freq_dict[ngram] += 1
            json.dump(freq_dict, logh, indent=4)
        print('N-gram summary complete for', flag)
        # print(f'Top {top_size} {n}-grams ({flag}):')
        # pprint(cls.get_sorted_list(freq_dict)[:top_size])
        # pprint(cls.get_sorted_list(freq_dict)[-top_size:])

    @classmethod
    def summarize_data(cls, summary_type=''):
        # shape of the data
        print('Train total points: ', cls.train_raw.shape[0])
        print('Test total points: ', cls.test_raw.shape[0])
        print('Number of sincere questions:', len(cls.train_raw[cls.train_raw['target'] == 0]))
        print('Number of insincere questions:', len(cls.train_raw[cls.train_raw['target'] == 1]))
        print('Percentage:',
              len(cls.train_raw[cls.train_raw['target'] == 1]) * 100 / len(cls.train_raw[cls.train_raw['target'] == 0]))
        cls.clean_non_ascii_voc()
        print('Data points after cleanup:', len(cls.train_raw['question_text']))
        print(cls.train_raw['question_text'][:20])

        if summary_type == 'full':
            cls.n_gram_summary(1, cls.train_raw[cls.train_raw['target'] == 0], 'sincere', 10)
            cls.n_gram_summary(1, cls.train_raw[cls.train_raw['target'] == 1], 'insincere')
            cls.n_gram_summary(2, cls.train_raw[cls.train_raw['target'] == 0], 'sincere', 10)
            cls.n_gram_summary(2, cls.train_raw[cls.train_raw['target'] == 1], 'insincere', 10)
            cls.n_gram_summary(3, cls.train_raw[cls.train_raw['target'] == 0], 'sincere', 10)
            cls.n_gram_summary(3, cls.train_raw[cls.train_raw['target'] == 1], 'insincere', 10)

        if summary_type == 'plot':
            pass

    # source: https://www.kaggle.com/christofhenkel/how-to-preprocessing-when-using-embeddings
    @classmethod
    def word_filter(cls):
        misspell_dict = {
            'colour': 'color',
            'centre': 'center',
            'didnt': 'did not',
            'doesnt': 'does not',
            'isnt': 'is not',
            'wasnt': 'was not',
            'arent': 'are not',
            'wouldnt': 'would not',
            'shouldnt': 'should not',
            'hasnt': 'has not',
            'favourite': 'favorite',
            'travelling': 'traveling',
            'counselling': 'counseling',
            'theatre': 'theater',
            'cancelled': 'canceled',
            'labour': 'labor',
            'organisation': 'organization',
            'wwii': 'world war 2',
            'citicise': 'criticize',
            'instagram': 'social medium',
            'whatsapp': 'social medium',
            'snapchat': 'social medium',
            'facebook': 'social medium',
            'twitter': 'social medium',
            'reddit': 'social medium',
            ' a ': ' ',
            ' to ': ' ',
            ' of ': ' ',
            ' and ': ' '
        }

        def replace(match):
            return misspell_dict[match.group(0)]

        misspell_reg = re.compile('(%s)' % '|'.join(misspell_dict.keys()))
        # run twice to remove repeated occurrences
        cls.train_raw['question_text'] = cls.train_raw['question_text'].apply(lambda x: misspell_reg.sub(replace, x))
        cls.train_raw['question_text'] = cls.train_raw['question_text'].apply(lambda x: misspell_reg.sub(replace, x))
        print('Word filtering complete.')
        cls.__save_preprocessed_corpus()
