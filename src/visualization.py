from data import Data
from pprint import pprint
from keras.utils.vis_utils import plot_model
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import json
import pandas as pd
import numpy as np
import plotly.figure_factory as ff
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import matplotlib.style
import matplotlib as mpl
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import seaborn as sns


class DataVisualization(object):
    graphics_path = '/home/andrei/quora-research/graphics/'
    log_path = '/home/andrei/quora-research/logs/'
    mpl.style.use('seaborn-deep')

    # Source: https://www.kaggle.com/aashita/word-clouds-of-various-shapes
    @classmethod
    def plot_wordcloud(cls, text=pd.DataFrame(), mask=None, max_words=900, max_font_size=100, figure_size=(8.0, 5.0),
                       title=None, title_size=30, image_color=False, log_flag=''):
        text = ' '.join(text.values)
        stopwords = set(STOPWORDS)
        more_stopwords = {'one', 'br', 'Po', 'th', 'sayi', 'fo', 'Unknown'}
        stopwords = stopwords.union(more_stopwords)

        wordcloud = WordCloud(background_color='black',
                              stopwords=stopwords,
                              max_words=max_words,
                              max_font_size=max_font_size,
                              random_state=42,
                              width=600,
                              height=300,
                              mask=mask)
        wordcloud.generate(str(text))

        plt.figure(figsize=figure_size)
        if image_color:
            image_colors = ImageColorGenerator(mask)
            plt.imshow(wordcloud.recolor(color_func=image_colors), interpolation="bilinear")
            plt.title(title, fontdict={'size': title_size,
                                       'verticalalignment': 'bottom'})
        else:
            plt.imshow(wordcloud)
            plt.title(title, fontdict={'size': title_size, 'color': 'black',
                                       'verticalalignment': 'bottom'})
        plt.savefig(cls.graphics_path + '_wordcloud_' + log_flag + '.png', bbox_inches='tight')
        plt.axis('off')
        plt.tight_layout()
        plt.show()

    @classmethod
    def __horizontal_bar_chart(cls, df):
        trace = go.Bar(
            y=df["word"].values[::-1],
            x=df["wordcount"].values[::-1],
            showlegend=False,
            orientation='h',
            marker=dict(
                color=df["wordcount"].values[::-1],
                colorscale='YlOrRd'
            ),
        )
        return trace

    @classmethod
    def plot_datapoint_frequency(cls):
        file_list = ['n_gram_1_insincere.json',
                     'n_gram_1_sincere.json',
                     'n_gram_2_insincere.json',
                     'n_gram_2_sincere.json',
                     'n_gram_3_insincere.json',
                     'n_gram_3_sincere.json'
                     ]
        for file in file_list:
            log_file, graph_file = cls.log_path + file, cls.graphics_path + file.split('.')[0] + '.png'
            with open(log_file, 'r') as lfh, open(graph_file, 'w') as gfh:
                data = json.load(lfh)
                fd_sorted = pd.DataFrame(sorted(data.items(), key=lambda x: x[1])[::-1])
                fd_sorted.columns = ['word', 'wordcount']
                trace = cls.__horizontal_bar_chart(fd_sorted.head(50))
                fig = go.Figure(data=[trace], layout={'title': {'text': ' '.join(x.capitalize() for x in file.split('.')[0].split('_'))}})
                fig.show()
                # fig.write_image(gfh)
        print("Frequency plot files generated.")

    @classmethod
    def plot_question_length_distribution(cls):
        len_sincere = Data.train_raw[Data.train_raw['target'] == 0]['question_text'].apply(lambda x: len(x))
        len_insincere = Data.train_raw[Data.train_raw['target'] == 1]['question_text'].apply(lambda x: len(x))

        sns.distplot(len_sincere, label='Sincere questions', axlabel='Question length')
        dist_plot = sns.distplot(len_insincere, label='Insincere questions', axlabel='Question length')
        fig = dist_plot.get_figure()
        plt.legend()
        plt.show()
        fig.savefig(f'{cls.graphics_path}question_length_distribution.png')

    @classmethod
    def plot_sincere_insincere_ratio(cls):
        labels = 'Sincere', 'Insincere'
        num_sincere = len(Data.train_raw[Data.train_raw['target'] == 0])
        num_insincere = len(Data.train_raw) - num_sincere
        explode = (0, 0.1)  # only "explode" the 2nd slice
        fig, ax1 = plt.subplots()
        ax1.pie([num_sincere, num_insincere], explode=explode, labels=labels, autopct='%1.1f%%',
                shadow=True, startangle=90)
        ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
        plt.show()
        fig.savefig(f'{cls.graphics_path}sincere_insincere_ratio.png')

    @classmethod
    def plot_feature_histograms(cls):
        features = {
            'total_length': 'Sentence length',
            'capitals': 'Capital letters',
            'caps_vs_length': 'Ratio of capital letters to sentence length',
            'num_words': 'Number of words',
            'num_unique_words': 'Number of unique words',
            'words_vs_unique': 'Ratio of unique words',
        }
        for feature in features:
            sns.distplot(
                Data.train_raw[Data.train_raw['target'] == 0][feature], label='Sincere questions', axlabel=features[feature])
            dist_plot = sns.distplot(
                Data.train_raw[Data.train_raw['target'] == 1][feature], label='Insincere questions', axlabel=features[feature], color='orange')
            fig = dist_plot.get_figure()
            plt.legend()
            plt.show()
            fig.savefig(f'{cls.graphics_path}{feature}_distribution.png')


class ModelVisualization(object):
    graphics_path = '/home/andrei/quora-research/graphics/models/'

    @classmethod
    def plot_confusion_matrix(cls, matrix, title):
        z = [matrix[1], matrix[0]]
        z_text = [['{0:.3f}'.format(x) for x in l] for l in z]
        fig = ff.create_annotated_heatmap(
            z=z,
            x=['Sincere', 'Insincere'],
            y=['Insincere', 'Sincere'],
            annotation_text=z_text, colorscale='Blues')
        fig['layout'].update(title=title)
        fig.layout.update(
            go.Layout(
                autosize=False,
                title=title,
                font=dict(size=18)
            )
        )
        fig.show(title=title)

    @classmethod
    def __plot_confusion_matrix_old(cls, matrix, accuracy, title):
        mpl.rcParams.update(mpl.rcParamsDefault)
        pprint(matrix)
        print(f'Accuracy: {accuracy}')
        cm_df = pd.DataFrame(matrix,
                             index=['sincere', 'insincere'],
                             columns=['sincere', 'insincere'])
        plt.figure(figsize=(10, 10))
        plt.tight_layout()
        ax = sns.heatmap(cm_df, annot=True, linewidths=.5, square=True)
        # ax.figure.tight_layout()
        plt.title(f'{title} \nAccuracy:{accuracy}')
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.savefig(f'{cls.graphics_path + "_".join(title.split(" "))}')
        plt.show()

    @classmethod
    def generate_model_diagram(cls, keras_model, name):
        plot_model(keras_model, to_file=f'{cls.graphics_path + name}.png', show_shapes=True, show_layer_names=True)


def custom_bert_cms():
    cms = {
        (4000, 32): [[963, 37], [354, 646]],
        (8000, 32): [[3469, 531], [346, 3654]],
        (20000, 16): [[8760, 1240], [652, 9348]],
        (35000, 16): [[15650, 1850], [1366, 16134]]
    }
    for stats in cms:
        cm = cms[stats]
        title = f'BERT {stats[0]} questions, batch {stats[1]}'
        cm = np.array([np.array(xi) for xi in cm])
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        ModelVisualization.plot_confusion_matrix(cm, title)

# custom_bert_cms()


'''
plotly color scales:
[‘Blackbody’,
‘Bluered’,
‘Blues’,
‘Earth’,
‘Electric’,
‘Greens’,
‘Greys’,
‘Hot’,
‘Jet’,
‘Picnic’,
‘Portland’,
‘Rainbow’,
‘RdBu’,
‘Reds’,
‘Viridis’,
‘YlGnBu’,
‘YlOrRd’]
'''