import pandas as pd

def balance_dataset(ds):
    print(f'Target 1 size:{len(ds[ds.target == 0])}\nTarget 2 size:{len(ds[ds.target == 1])}')
    common_size = min(len(ds[ds.target == 0]), len(ds[ds.target == 1]))
    print(f'Common size: {common_size}')
    new_ds1 = ds[ds.target == 1].sample(n=common_size, replace=False)
    new_ds2 = ds[ds.target == 0].sample(n=common_size, replace=False)
    return new_ds1.append(new_ds2).sample(frac=1)

def main():
    ds = pd.read_csv('../data/train.csv')
    balanced = balance_dataset(ds)
    print(balanced)
    balanced.to_csv('../data/balanced_train.csv', index=False, header=True)

if __name__ == '__main__':
    main()
