from visualization import DataVisualization
from data import Data
from model import DataRepresentation, BiGRUModel, BiLSTMModel, AttentionModel
from pprint import pprint


class Research(object):

    @classmethod
    def __data_viz(cls):
        # DataVisualization.plot_wordcloud(Data.train_raw[Data.train_raw['target'] == 0], title='Sincere questions',
        #                                  log_flag='sincere')
        # DataVisualization.plot_wordcloud(Data.train_raw[Data.train_raw['target'] == 1], title='Insincere questions',
        #                                  log_flag='insincere')
        # DataVisualization.plot_datapoint_frequency()
        DataVisualization.plot_question_length_distribution()
        DataVisualization.plot_sincere_insincere_ratio()

    @classmethod
    def __data_summary(cls):
        # Data.summarize_data('full')
        Data.vocabulary_embedding_overlap('word2vec')
        Data.vocabulary_embedding_overlap('paragram')
        Data.vocabulary_embedding_overlap('fasttext')
        Data.vocabulary_embedding_overlap('glove')

    @classmethod
    def __run_baseline_with_all_embeddings(cls):
        for em in ['word2vec', 'paragram', 'fasttext', 'glove']:
        # for em in ['word2vec']:
            DataRepresentation.configure_representation({
                'embedding_type': em,
                'max_features': 105000,
                'embed_size': 300,
                'max_len': 70,
                'random_seed': 42,
                'balanced': False
            })

            BiGRUModel.set_hyperparameters(epochs=1)
            BiGRUModel.run()

            BiLSTMModel.set_hyperparameters(epochs=1)
            BiLSTMModel.run()

    @classmethod
    def __run_attention_with_all_embeddings(cls):
        for em in ['glove']:
        # for em in ['word2vec', 'paragram', 'fasttext', 'glove']:
            DataRepresentation.configure_representation({
                'embedding_type': em,
                'max_features': 105000,
                'embed_size': 300,
                'max_len': 100,
                'random_seed': 42,
                'balanced': False
            })
            AttentionModel.set_hyperparameters(epochs=1)
            AttentionModel.run()

    @classmethod
    def start(cls):
        # Data.load_data('all')
        # Data.augment_features()
        # Data.clean_non_ascii_voc()
        # Data.word_filter()

        Data.load_preprocessed(aug=True)
        # cls.__run_baseline_with_all_embeddings()
        cls.__run_attention_with_all_embeddings()

        # cls.__run_baseline_with_all_embeddings()
        # DataVisualization.plot_feature_histograms()
        # cls.__data_viz()
        # cls.__data_summary()
        # cls.__run_baseline_with_all_embeddings()
        #
        # Data.load_embeddings('wiki')
        # Data.show_embedding_info('wiki')
        #
        # Data.load_embeddings('glove')
        # Data.show_embedding_info('glove')

        # Data.load_data('all')
        # Data.clean_non_ascii_voc()
        # Data.word_filter()
        # Data.load_embeddings('word2vec')

        # cls.__data_summary()
        # cls.__data_viz()

        # Representation.init(['keras'])
        # FirstModel.run()


def main():
    Research.start()


if __name__ == '__main__':
    main()

"""
Potential features

number of one letter tokens
number of capitalized letters
number of URLS
number  of  tokens  with  non-alpha  characters  in  the middle
number of discourse connectives, based on [13]
number of politeness words
number of modal words (to measure hedging and confidence by speaker)
number of unknown words as compared to a dictionary of English words (meant to measure uniqueness and any misspellings)
number of insult and hate blacklist words
length of comment in tokens
average length of word
number of punctuations
number  of  periods,  question  marks,  quotes,  and  repeated punctuation

ClearNLP

"""

"""
Models to try:
Vowpital Wabbit regression
https://www.kaggle.com/kashnitsky/vowpal-wabbit-tutorial-blazingly-fast-learning
"""
